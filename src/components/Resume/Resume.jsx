import React, { Fragment, useState } from 'react';

import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { pdfjs, Document, Page } from 'react-pdf';
import pathPdf from '../../assets/docs/ResumeOnline.pdf';
import Button from '@material-ui/core/Button';
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;
import './resume.css';

function Resume() {
  const [pageNum, setPageNum] = useState(1);
  return (
    <Fragment>
      <Grid
        container
        direction="column"
        justify="space-evenly"
        alignItems="center"
        spacing={3}
      >
        <Grid item>
          <Typography
            align="center"
            variant="h3"
            style={{ textAlign: 'center' }}
            color="primary"
            children="Resume"
          />
        </Grid>
        <Grid item className="resumepdf">
          <Grid
            container
            direction="row"
            justify="space-evenly"
            alignItems="center"
            spacing={3}
          >
            <Grid item>
              <Document file={pathPdf}>
                <Page pageNumber={pageNum} />
              </Document>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <a href={pathPdf} download="JorgeLMendoza" className="downloadLink">
            <Button
              style={{
                margin: '0 auto',
                top: '10px',
                display: 'block',
                position: 'static',
              }}
              variant="contained"
              color="secondary"
              children="Download"
            />
          </a>
        </Grid>
      </Grid>
    </Fragment>
  );
}

export default Resume;
