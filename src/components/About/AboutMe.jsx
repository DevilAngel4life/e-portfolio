import React, { Fragment } from 'react';

import Typography from '@material-ui/core/Typography';
import { Grid, useMediaQuery } from '@material-ui/core';
import 'typeface-roboto';

import ProImage from '../../assets/images/Pro.jpg';
import '../Styles/AboutMe.css';
import { lineSplit } from '../../utilities/lineSplit';

const AboutMeText =
  'Hello, and welcome to my E-Portfolio. My name is Jorge I have a major in computer science at the University of Idaho. While in school I had the opportunity to work on various classroom projects and outside projects not related to the University. A great opportunity I had was being able to work in the field with Fenway Group Company on campus while in school. I enjoy working out camping, hiking, and recently started biking as an alternative activity.';

const info = ' Jorge L Mendoza\n B.S in Computer Science \n University of Idaho';
// split the line of the text content
const infoAboutSectification = lineSplit(info);

function AboutMe(props) {
  // this is using the query size for mobile version on the image
  const matches = useMediaQuery('(min-width:634px)');

  return (
    <Fragment>
      <Grid container spacing={4} direction="column" alignItems="center">
        <Grid item>
          <Typography
            color="primary"
            align="center"
            variant="h3"
            style={{ textAlign: 'center' }}
            children="About Me"
          />
        </Grid>
        <Grid item>
          <Grid
            container
            spacing={6}
            alignItems="center"
            direction={matches ? 'row' : 'column'}
          >
            {' '}
            <Grid item>
              <img src={ProImage} id="image-Pro" id="Pro-Image" alt="Me" />
            </Grid>
            <Grid item>
              <Typography
                color="textSecondary"
                variant="h5"
                children={infoAboutSectification}
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <Typography color="textSecondary" variant="h5" children={AboutMeText} />
        </Grid>
      </Grid>
    </Fragment>
  );
}

export default AboutMe;
