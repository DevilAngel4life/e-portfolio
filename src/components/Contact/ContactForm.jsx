import React, { Fragment } from "react";
import { Grid } from "@material-ui/core";
import { SocialIcon } from "react-social-icons";

export default function ContactForm() {
  return (
    <Fragment>
      <Grid
        container
        direction="row"
        justify="space-evenly"
        alignItems="center"
        spacing={3}
      >
        <Grid item>
          <SocialIcon url="mailto:jorge498@live.com" />
        </Grid>
        <Grid item>
          <SocialIcon url="https://linkedin.com/in/jorgel-mendoza"/>
        </Grid>
        <Grid item>
          <SocialIcon url="https://gitlab.com/DevilAngel4life"/>
        </Grid>
        <Grid item>
          {" "}
          <SocialIcon url="https://github.com/DevilAngel4life"/>
        </Grid>
      </Grid>
    </Fragment>
  );
}
