import React, { Fragment } from 'react';
import { Grid } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import ContactForm from './contactForm';

const ContactMe = () => {
  return (
    <Fragment>
      <Grid
        container
        direction="column"
        justify="space-evenly"
        alignItems="center"
        spacing={3}
      >
        <Grid item>
          <Typography
            color="primary"
            align="center"
            variant="h3"
            style={{ textAlign: 'center' }}
            children="Contact Me"
          />
        </Grid>
        <Grid item color="red">
          <Grid
            container
            direction="row"
            justify="space-evenly"
            alignItems="center"
            spacing={3}
          >
            <Grid item>
              <ContactForm />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default ContactMe;
