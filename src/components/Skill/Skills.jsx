import React, { Fragment } from "react";

import { Grid, CardMedia, makeStyles } from "@material-ui/core";
import nod from "../../assets/images/nodejs.png";

const useStyles = makeStyles({
  node: {
    width: 200,
    height: 120
  }
});

function importAll(r) {
  return r.keys().map(r);
}

const listOfSkills = importAll(
  require.context(
    "../../assets/images/skills",
    false,
    /\.(png|jpe?g|svg)$/
  )
);

const Skills = props => {
  const classes = useStyles();
  return (
    <Fragment>
      <Grid
        container
        direction="row"
        justify="space-evenly"
        alignItems="center"
        spacing={3}
      >
        <Grid item>
          <CardMedia className={classes.node} image={nod} />
        </Grid>
        {listOfSkills.map((image, index) => (
          <Grid item key={index} color="red">
            <CardMedia
              className="SkillsImages"
              key={index}
              image={image}
            />
          </Grid>
        ))}
      </Grid>
    </Fragment>
  );
};

export default Skills;
