import React from "react";
import { Grid } from "@material-ui/core";

import Typography from "@material-ui/core/Typography";
import Skills from "./Skills";
import "../Styles/Skills.css";

const SkillsContent = props => {
  return (
    <div>
      <Grid
        container
        direction="row"
        justify="space-evenly"
        alignItems="center"
        spacing={3}
      >
        <Grid item>
          <Typography
            color="primary"
            align="center"
            variant="h3"
            style={{ textAlign: "center" }}
          >
            Skills
          </Typography>
        </Grid>
        <Grid item color="red">
          <Skills></Skills>
        </Grid>
      </Grid>
    </div>
  );
};

export default SkillsContent;
