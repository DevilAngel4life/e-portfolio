import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import {
  Card,
  CardContent,
  CardMedia,
  CardActions,
  Typography,
  CardActionArea,
  Button,
} from '@material-ui/core';
import './projects.css';

CardProject.prototype = {
  tittle: PropTypes.string,
  image: PropTypes.string,
  content: PropTypes.string,
  color: PropTypes.string,
  cardAction1: PropTypes.string,
  link1: PropTypes.string,
};

/**
 * @Description The conponent Returns a card Content with the properties
 * that were passed to the component
 * @prop tittle - String type For name of Card
 * @prop image - String location for the image to be used as contenc
 * @prop contant - String containing the content to be displeyed
 * @prop color - color of the Actions Button
 * @prop cardAction1 - String name of the action1
 */
export function CardProject({ tittle, image, content, color, cardAction1, link1 }) {
  return (
    <Fragment>
      <Card className="cardColors">
        <CardMedia
          component="img"
          alt="Contemplative Reptile"
          image={image}
          title={tittle}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2" children={tittle} />
          <Typography variant="body2" color={color} component="p" children={content} />
        </CardContent>
        <CardActions>
          <Button size="small" color="primary" href={link1} children={cardAction1} />
        </CardActions>
      </Card>
    </Fragment>
  );
}
