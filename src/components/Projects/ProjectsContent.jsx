import React, { Fragment } from "react";

import { Grid, Typography } from "@material-ui/core";
import Dbot from "../../assets/images/DiscordBot.png";
import PongPlay from "../../assets/images/GamePlay.png";
import Comp from "../../assets/images/com.png";
import CEnd from "../../assets/images/chasingTheEnd.png";
import FW from "../../assets/images/FW.png";
import Bio from "../../assets/images/Bio.png";

import projectsInfo from "./projectsInfo.json";

import { CardProject } from "./CardProject";
import "./projects.css";

export const Projects = () => {
  return (
    <Fragment>
      <Grid
        container
        direction="column"
        justify="space-evenly"
        alignItems="center"
        spacing={3}
      >
        <Grid item>
          <Typography
            color="primary"
            align="center"
            variant="h3"
            style={{ textAlign: "center" }}
          >
            Projects
          </Typography>
        </Grid>
        <Grid item color="red">
          <Grid
            container
            direction="row"
            justify="space-evenly"
            alignItems="flex-start"
            spacing={3}
          >
            <Grid item>
              <CardProject
                tittle={projectsInfo.DiscordBot.tittle}
                image={Dbot}
                content={projectsInfo.DiscordBot.content}
                color="textSecondary"
                cardAction1={projectsInfo.DiscordBot.action}
                link1={projectsInfo.DiscordBot.link}
              ></CardProject>
            </Grid>
            <Grid item>
              {" "}
              <CardProject
                tittle={projectsInfo.FenWhere.tittle}
                image={FW}
                content={projectsInfo.FenWhere.content}
                color="textSecondary"
                cardAction1={projectsInfo.FenWhere.action}
                link1={projectsInfo.FenWhere.link}
              ></CardProject>
            </Grid>
            <Grid item>
              {" "}
              <CardProject
                tittle={projectsInfo.Compiler.tittle}
                image={Comp}
                content={projectsInfo.Compiler.content}
                color="textSecondary"
                cardAction1={projectsInfo.Compiler.action}
                link1={projectsInfo.Compiler.link}
              ></CardProject>
            </Grid>
            <Grid item>
              {" "}
              <CardProject
                tittle={projectsInfo.ChaseEnd.tittle}
                image={CEnd}
                content={projectsInfo.ChaseEnd.content}
                color="textSecondary"
                cardAction1={projectsInfo.ChaseEnd.action}
                link1={projectsInfo.ChaseEnd.link}
              ></CardProject>
            </Grid>
            <Grid item>
              {" "}
              <CardProject
                tittle={projectsInfo.BioDiesel.tittle}
                image={Bio}
                content={projectsInfo.BioDiesel.content}
                color="textSecondary"
                cardAction1={projectsInfo.BioDiesel.action}
                link1={projectsInfo.BioDiesel.link}
              ></CardProject>
            </Grid>
            <Grid item>
              {" "}
              <CardProject
                tittle={projectsInfo.Superpong.tittle}
                image={PongPlay}
                content={projectsInfo.Superpong.content}
                color="textSecondary"
                cardAction1={projectsInfo.Superpong.action}
                link1={projectsInfo.Superpong.link}
              ></CardProject>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Fragment>
  );
};
