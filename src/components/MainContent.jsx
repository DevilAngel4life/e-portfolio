import React from 'react';

import 'typeface-roboto';
import { Grid } from '@material-ui/core';
import './Styles/Layout/MainContentLayout.css';
import AboutMe from './About/AboutMe';
import SkillsContent from './Skill/SkillsContent';
import { Projects } from './Projects/ProjectsContent';
import ContactMe from './Contact/ContactMeContent';
import Resume from './Resume/Resume';

function MainContent(props) {
  return (
    <div className="Content_layout">
      <Grid
        id="ListSection"
        direction="column"
        container
        spacing={2}
        justify="space-around"
        alignItems="center"
      >
        <Grid item id="AboutItem">
          <AboutMe />
        </Grid>
        <Grid item id="ProjectItem" style={{ color: 'white' }}>
          <Projects />
        </Grid>
        <Grid item id="ResumeItem" style={{ color: 'white' }}>
          <Resume />
        </Grid>
        <Grid item id="SkillsItem">
          <SkillsContent />
        </Grid>
        <Grid item id="ContactItem" style={{ color: 'white' }}>
          <ContactMe />
        </Grid>
      </Grid>
    </div>
  );
}
export default MainContent;
