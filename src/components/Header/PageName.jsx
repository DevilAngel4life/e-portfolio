import React from "react";
import logoImgine from "../../assets/images/Logo.svg";

const PageName = () => {
  return (
    <React.Fragment>
      <img src={logoImgine} />
    </React.Fragment>
  );
};

export default PageName;
