import React from "react";
import PropTypes from "prop-types";
import Fab from "@material-ui/core/Fab";
import MenuIcon from "@material-ui/icons/Menu";
import { Button } from "@material-ui/core";

Button.protoType = {
  open: PropTypes.bool,
  toggleSwitch: PropTypes.func
};

const ButtonOptions = ({toggleSwitch, open}) => {
  return (
    <React.Fragment>
      <Fab
        color="primary"
        size="small"
        onClick={() => toggleSwitch(open)}
        aria-label="Add"
      >
        <MenuIcon />
      </Fab>
    </React.Fragment>
  );
};

export default ButtonOptions;
