import React, { useState } from "react";

import Grid from "@material-ui/core/Grid";

import "./HeaderContent.css";
import PageName from "./PageName";
import ButtonOptions from "./ButtonOptions";
import MenuOptions from "./MenuOptions";

function Header(props) {
  const [open, setOpen] = useState(false);

  function toggleSwitch(toggleCurrent) {
    return setOpen(!toggleCurrent);
  }

  return (
    <div className="HeaderBck">
      <Grid
        className={"Header-layout sticky backtransparent"}
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <PageName />
        <MenuOptions toggleSwitch={toggleSwitch} open={open} />
        <ButtonOptions toggleSwitch={toggleSwitch} open={open} />
      </Grid>
    </div>
  );
}

export default Header;
