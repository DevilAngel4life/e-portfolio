import React, { Fragment } from 'react';
import { Link } from 'react-scroll';
import PropTypes from 'prop-types';

import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';
import Slide from '@material-ui/core/Slide';
import { Paper, MenuItem, MenuList, ClickAwayListener } from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';
import './HeaderContent.css';

MenuOptions.propTypes = {
  open: PropTypes.bool,
  toggleSwitch: PropTypes.func,
};

const useStyles = makeStyles((theme) => ({
  buttonItems: 'padding-items',
}));

const scrollSettings = { smooth: 'linear', duration: 800 };

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="left" ref={ref} {...props} />;
});

function MenuOptions({ open, toggleSwitch }) {
  const classes = useStyles();

  return (
    <Fragment>
      <Dialog
        className="colorBack"
        open={open}
        maxWidth="xs"
        fullWidth={true}
        TransitionComponent={Transition}
        transitionDuration={{ enter: scrollSettings.duration, exit: scrollSettings.duration }}
      >
        <ClickAwayListener
          onClickAway={() => {
            toggleSwitch(open);
          }}
        >
          <Paper id="menu-button-color">
            <MenuList className="menu-items-content">
              <MenuItem className={classes.buttonItems}>
                <Link
                  to="AboutItem"
                  spy={true}
                  smooth={scrollSettings.smooth}
                  duration={scrollSettings.duration}
                  offset={-45}
                  onClick={() => {
                    toggleSwitch(open);
                  }}
                >
                  <Typography variant="h5" children="About"/>
                </Link>
              </MenuItem>
              <MenuItem className={classes.buttonItems}>
                <Link
                  to="ProjectItem"
                  spy={true}
                  smooth={scrollSettings.smooth}
                  duration={scrollSettings.duration}
                  offset={-70}
                  onClick={() => {
                    toggleSwitch(open);
                  }}
                >
                  <Typography variant="h5" children="Projects"/>
                </Link>
              </MenuItem>
              <MenuItem className={classes.buttonItems}>
                <Link
                  to="ResumeItem"
                  spy={true}
                  smooth={scrollSettings.smooth}
                  duration={scrollSettings.duration}
                  offset={-45}
                  onClick={() => {
                    toggleSwitch(open);
                  }}
                >
                  <Typography variant="h5" children="Resume"/>
                </Link>
              </MenuItem>
              <MenuItem className={classes.buttonItems}>
                <Link
                  to="SkillsItem"
                  spy={true}
                  smooth={scrollSettings.smooth}
                  duration={scrollSettings.duration}
                  onClick={() => {
                    toggleSwitch(open);
                  }}
                >
                  <Typography variant="h5" children="Skills"/>
                </Link>
              </MenuItem>
              <MenuItem className={classes.buttonItems}>
                <Link
                  to="ContactItem"
                  spy={true}
                  smooth={scrollSettings.smooth}
                  duration={scrollSettings.duration}
                  onClick={() => {
                    toggleSwitch(open);
                  }}
                >
                  <Typography variant="h5" children="Contact Me"/>
                </Link>
              </MenuItem>
            </MenuList>
          </Paper>
        </ClickAwayListener>
      </Dialog>
    </Fragment>
  );
}

export default MenuOptions;
