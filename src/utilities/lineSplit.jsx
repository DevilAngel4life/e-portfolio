import React from "react";

export const lineSplit = (text) => {
  return text.split("\n").map((item, i) => {
    return <div key={i}>{item}</div>;
  });
};
