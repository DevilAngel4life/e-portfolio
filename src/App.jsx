import React, { useState } from 'react';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import Header from './components/Header/Header';
import MainContent from './components/MainContent';
import './components/Styles/Layout/AppLayout.css';
import 'typeface-roboto';

/*
  This are the theme for the entire application here i have access to 
  change all of the color fades shadows etc. I So i don't have  to change 
  them all throught the application. I can also set a dark mode or light 
  mode default is the light mode. To find other options just 
  console.log(theme) to view properties
*/
const theme = createMuiTheme({
  palette: {
    primary: { main: '#66FCF1' },
    secondary: {
      main: '#EDF5E1',
    },
    text: {
      primary: '#66FCF1',
      secondary: '#C5C6C7',
    },
  },
});

function App() {
  return (
    <React.Fragment>
      <MuiThemeProvider theme={theme}>
        <div className="Main-Layout">
          <Header />
          <MainContent />
        </div>
      </MuiThemeProvider>
    </React.Fragment>
  );
}

export default App;
