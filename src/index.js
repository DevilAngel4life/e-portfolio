import React from "react";
import ReactDOM from "react-dom";
// import CssBaseline from "@material-ui/core/CssBaseline";

// const dotenv = require("dotenv");
// dotenv.config();

if (process.env.NODE_ENV !== "production") {
  console.log("Looks like we are in development mode!");
}

import App from "./App";

ReactDOM.render(
  <React.Fragment>
    <App />
  </React.Fragment>,
  document.getElementById("root")
);
