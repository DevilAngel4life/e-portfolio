const merge = require("webpack-merge");
const common = require("./webpack.common.js");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = merge(common, {
  mode: "production",
  devtool: "none"
  // ,
  // plugins: [
  //   new HtmlWebpackPlugin({
  //     minify: {
  //       removeComments: true,
  //       // collapseWhitespace: true,
  //       // removeRedundantAttributes: true,
  //       // useShortDoctype: true,
  //       // removeEmptyAttributes: true,
  //       // removeStyleLinkTypeAttributes: true,
  //       keepClosingSlash: true,
  //       minifyJS: true,
  //       minifyCSS: true,
  //       minifyURLs: true
  //     }
  //   })
  //   // ,
  //   // new ManifestPlugin({
  //   //   fileName: 'asset-manifest.json',
  //   // })
  // ]
});
