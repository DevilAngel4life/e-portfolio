const merge = require("webpack-merge");
const common = require("./webpack.common.js");

module.exports = merge(common, {
  mode: "development",
  devtool: "inline-source-map",
  devServer: {
    open: false, //This will prevent from opening a new tab on the browser every time
    contentBase: "./dist"
  }
});
